import React, { Component } from 'react';

import styles from '../../styles/riderBusiness.css'
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';
import { browserHistory } from 'react-router'
import firebase from 'firebase'
const FormItem = Form.Item;
const Option = Select.Option;


class BusinessDetail extends React.Component {
  state = {
  };
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        // firebase.database().ref().child("rider")
        // .child(firebase.auth().currentUser.uid).push({businessDetails:values}).then(()=>console.log("done"))
        // browserHistory.push('/riderDashboard')
        // console.log('Received values of form: ', values);
        firebase.database().ref("user" + "/" + firebase.auth().currentUser.uid).update(
          JSON.parse(JSON.stringify({ "businessDetail": values }))
        ).then(() => browserHistory.push('/rider'))
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '20',
    })(
      <Select style={{ width: 70 }}>
        <Option value="1">+1</Option>
        <Option value="20">+20</Option>
        <Option value="92">+92</Option>
      </Select>
    );
    return (
      <div
        style={{
          justifyContent: "center"
        }}
      >
        <Form onSubmit={this.handleSubmit} className="business-form">
          <FormItem
            label={(
              <span
                style={{ color: "white" }}
              >
                Business Name&nbsp;
              <Tooltip title="Your Business Name">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
          >
            {getFieldDecorator('businessName', {
              rules: [{ required: true, message: 'Please input your Business Name!', whitespace: true }],
            })(
              <Input 
              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>
          <FormItem
            label={(
              <span style={{ color: "white" }}>
                Business Address
            </span>
            )}
          >
            {getFieldDecorator('address', {
              rules: [{ required: true, message: 'Please select your Business Address' }],
            })
              (
              <Input 
              style={{
                textAlign:"right"
              }}
              />
              )}
          </FormItem>
          <FormItem
            label={(
              <span style={{ color: "white" }}>
                Phone Number 1
            </span>
            )}
          >
            {getFieldDecorator('phone1', {
              rules: [{ required: true, message: 'Please input your phone number!' }],
            })(
              <Input addonBefore={prefixSelector} style={{ width: '100%' }} 
              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>
          <FormItem
            label={(
              <span style={{ color: "white" }}>
                Phone Number 2
            </span>
            )}
          >
            {getFieldDecorator('phone2', {
              rules: [{ message: 'Please input your phone second number!' }],
            })(
              <Input addonBefore={prefixSelector} style={{ width: '100%' }} 
              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>
          <FormItem
            label={(
              <span style={{ color: "white" }}>
                General Information
            </span>
            )}
          >
            {getFieldDecorator('generalInformation', {
              rules: [{ message: 'Please provide your general business information details' }],
            })(
              <Input.TextArea
                style={{ borderRadius: 25,textAlign:"right", backgroundColor: "#626268", color: "white" }}
                rows="7"
                placeholder="Please provide your general business information details"
              />)}
          </FormItem>
          <FormItem {...tailFormItemLayout}>
            <Button ghost htmlType="submit">Subnit</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

const RiderBusiness = Form.create()(BusinessDetail);

export default RiderBusiness