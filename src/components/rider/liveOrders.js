import React ,{Component} from 'react';
import firebase from 'firebase'
import {Card,Button,Icon, Avatar,Modal} from 'antd'
const confirm = Modal.confirm
class LiveOrders extends Component{
    constructor(){
        super();
        this.state={arr:[]}
    }
    showConfirm() {
        confirm({
          title: 'Do you Want to cancel this order?',
          content: 'Are you sure',
          onOk() {
            alert('OK');
          },
          onCancel() {
            alert('Cancel');
          },
        });
      }
    cancelOrder(){
        this.showConfirm()
    }

    componentDidMount(){
        firebase.database().ref().child("orders").on("value",(snap)=>{
            console.log(snap.val())
            var values = snap.val();
            var arr=[];
            for(var i in values){
                arr.push(values[i])
            }
            this.setState({
                arr:arr.reverse()
            })
            console.log(arr,"array")
        })
    }
    render(){
        return(
            <div>
                {!this.state.arr.length ? 
                <div style={{color:"white"}}><b><i>No orders yet...</i></b></div> :
                    this.state.arr.map((data, index) =>
                    <div
                        key={index}
                    >
                        <Card
                            loading={false}
                            bordered={true}
                            hoverable={true}
                            style={{
                                backgroundColor: "#655E68",
                                width: "80%",
                                marginRight: "auto",
                                marginLeft: "auto",
                                borderColor: "#272727"
                            }}
                            actions={[
                            <Button 
                            disabled={data.status !== "NEW" ? true :false}
                            onClick={this.cancelOrder.bind(this)}
                            >Cancel</Button>,
                            ]}
                        >
                            <div
                                style={{ color: "white" }}
                            >
                                <table>
                                    <thead>
                                        <tr>
                                            <th>From:</th>
                                            <th>{data.from}</th>
                                        </tr>
                                        <tr>
                                            <th>To:</th>
                                            <th>{data.to}</th>
                                        </tr>
                                        <tr>
                                            <th colSpan={2}>{new Date(data.postedAt).toLocaleString()}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{data.currency}:</td>
                                            <td><h4 style={{ color: "white" }}><i>{data.cost}</i></h4></td>
                                        </tr>
                                        <tr>
                                            <td>Must Have:</td>
                                            <td>{data.mustHave}</td>
                                        </tr>
                                        <tr>
                                            <td>Prefer to  Have:</td>
                                            <td>{data.preferToHave}</td>
                                        </tr>
                                        <tr>
                                            <td>Status:</td>
                                            <td>{data.status}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </Card>
                        <br />
                    </div>
                )}
            </div>

        )
    }
} 
export default LiveOrders