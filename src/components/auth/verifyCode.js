import React ,{Component} from 'react'
import PinInput from 'react-pin-input';
import {browserHistory} from 'react-router'
import { Form,Icon, Input, Button,Radio, Checkbox } from 'antd';
import firebase from 'firebase'
import store from '../../store'
import authActions from '../../store/actions/authActions'
// import {connect} from 'react-redux'

const FormItem = Form.Item;


class VerifyCode extends Component{
    constructor(){
        super();
        this.state={}
        this.values = window.credentials
    }
    verify(code){
        console.log(code,"##")
        window.confirmationResult.confirm(code).then((user)=>{
            console.log("verified",user);
            firebase.auth().currentUser.updateProfile({ displayName: this.values.name })
            .then(()=>{
                firebase.database().ref("user")
                .child(firebase.auth().currentUser.uid).set({
                    name:this.values.name,
                    phone:this.values.phone,
                    uid:firebase.auth().currentUser.uid,
                    type:this.values.userType
                })            // Response expired. Ask user to solve reCAPTCHA again.
                // ...
    
                store.dispatch(authActions.signUpSuccess(user))
                window.credentials.userType === "trucker"?
                browserHistory.push("/"+window.credentials.userType):
                browserHistory.push("/"+window.credentials.userType+"Info")                
                console.log(
                firebase.auth().currentUser.uid,
                firebase.auth().currentUser.displayName,
                firebase.auth().currentUser.phoneNummber
            )})
        })
        .catch((err)=>{
            console.log("ponka")
        store.dispatch(authActions.signUpFail(err))
        })
        console.log(window.credentials,"@@@@@@@@@@@@@")
    }
    handleSubmit = (e) => {
        e.preventDefault();
            const values= this.values
            console.log('Received values of form: ', this.values);
            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
              'size': 'normal',
              'callback': function(response) {
                firebase.auth().signInWithPhoneNumber(values.prefix+values.phone,window.recaptchaVerifier).then(
                  (confirmationResult)=>{
                    window.confirmationResult=confirmationResult;
                  console.log(window.confirmationResult,"this is magic")
                  }
                )
              },
              'expired-callback': function() {
              }
            }) 
      
          window.recaptchaVerifier.render().then(function(widgetId) {
          window.recaptchaWidgetId = widgetId;
      
          });
      }    
    render(){
        return(
            <div
            style={{alignItems:"center"}}
            >
                <center
                >
                    <PinInput
                        length={6}
                        onChange={(value) => { console.log(value) }}
                        type="numeric"
                        style={{ padding: '10px' }}
                        inputStyle={{ borderColor: 'red', backgroundColor: "#626268" }}
                        inputFocusStyle={{ borderColor: 'blue', backgroundColor: "white" }}
                        onComplete={(value) => this.verify(value)}
                    />
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <FormItem>
                            <Button onClick={()=>browserHistory.goBack()}
                            ghost
                            //  htmlType="submit" className="login-form-button"
                            >
                                Resend code
                            </Button>

                            <br />
                        </FormItem>
                        <div id="recaptcha-container" ref="recap"></div>
                    </Form>
                </center>
            </div>
        )
    }
}
export default VerifyCode