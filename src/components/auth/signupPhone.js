import React ,{Component} from 'react'
import { Form,Icon, Input, Button,Radio, Checkbox } from 'antd';
import styles from '../../styles/signupPhone.css';
import {browserHistory} from 'react-router'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import {signupPhone} from '../../store/middlewares/authMiddleware'
import firebase from 'firebase'
import Select from 'antd/lib/select';
const FormItem = Form.Item;
const Option = Select.Option
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const mapStateToProps = () =>({

})


const mapDispatchToProps = (dispatch)=>{console.log(dispatch)
  return bindActionCreators({
    signupPhone
  },dispatch
)
}

class SignUpPhone extends Component {
  constructor(props){
    super(props);
    this.state={visibility:"inline"}
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        window.credentials = values
        console.log('Received values of form: ', values);
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
          'size': 'normal',
          'callback': function(response) {
            // this.props.signupPhone()
            firebase.auth().signInWithPhoneNumber(values.prefix+values.phone,window.recaptchaVerifier).then(
              (confirmationResult)=>{
                window.confirmationResult=confirmationResult;
              console.log(window.confirmationResult,"this is magic")
              browserHistory.push("/verifyCode")  
              }
            )
          },
          'expired-callback': function() {
            alert("please refresh page")
          }
        }) 
  
      window.recaptchaVerifier.render().then(function(widgetId) {
      window.recaptchaWidgetId = widgetId;
  
      });
  
      }
    });
  }
//   verify(){
    
//     var code =this.state.code;
//     window.confirmationResult.confirm(code).then(()=>console.log("verified"))
//     .catch(()=>{console.log("ponka")})
  
// }
  render() {console.log("this.props",this.props)
  
    const { getFieldDecorator } = this.props.form;
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '+20',
    })(
      <Select style={{ width: 70 }}>
        <Option value="+20">Egypt</Option>
        <Option value="+92">Pakistan</Option>
        <Option value="+966">Saudi Arabia</Option>
        <Option value="+212">Morocco</Option>
        <Option value="+1">United States</Option>
      </Select>
    );
    
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
            <FormItem>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input your Full Name!' }],
              })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} type="text"  placeholder="Full Name" 
                style={{color:"white"}}
                />
              )}
            </FormItem>

                <FormItem
          label="Phone Number"
        >
          {getFieldDecorator('phone', {
            rules: [{ required: true, message: 'Please input your phone number!' }],
          })(
            <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
          )}
        </FormItem>
        <FormItem
        >
          {getFieldDecorator('userType'
          , {
            rules: [{ required: true, message: 'Please Select' }],
          })(
            <RadioGroup style={{color:"white"}}>
              <Radio checked={true} value="trucker"><span style={{color:"white",fontSize:"1.2em"}}>Trucker</span></Radio>
              <Radio value="rider"><span style={{color:"white",fontSize:"1.2em"}}>Rider</span></Radio>
            </RadioGroup>
          )}
        </FormItem>
        <FormItem>
          <Button ghost htmlType="submit" className="login-form-button"
          style={{display:this.state.visiblility}}
          >
            Send code
          </Button>

          <br/>
        </FormItem>
          <div id="recaptcha-container"></div>
      </Form>
    );
  }
}

const SignUpWithPhone = Form.create()(SignUpPhone);
export default connect(mapStateToProps,mapDispatchToProps)(SignUpWithPhone)