import React ,{Component} from 'react';
import {Header,VerifyCode} from '../components' 

class CodeVerify extends Component {
    render(){
        return(
            <div
            style={{
                backgroundColor:"#41404c",
                height:"100vh"
            }}
            >
            <Header/>
            <br/>
            <VerifyCode/>
            </div>
        )
    }
} export default CodeVerify