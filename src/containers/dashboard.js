import React ,{Component} from 'react';
import {Header} from '../components'
import {browserHistory} from 'react-router'
import {Button,Input,Affix} from 'antd'
import styles from '../styles/dashboard.css'
import style from '../App.css'
import firebase from 'firebase'
class Dashboard extends Component{
componentDidMount(){
    // window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
    //     'size': 'normal',
    //     'callback': function(response) {
    //       // reCAPTCHA solved, allow signInWithPhoneNumber.
    //       console.log(response)
    //       // ...
    //     },
    //     'expired-callback': function() {
    //       // Response expired. Ask user to solve reCAPTCHA again.
    //       // ...
    //     }
    //   }) 
    //   window.recaptchaVerifier.render().then(function(widgetId) {
    //     window.recaptchaWidgetId = widgetId;
    //   });
}
    render(){
        return(
            <div>
                <Affix>
                <Header/>
                </Affix>
            <div className="App" 
            >
                <div
                style={{
                    height:"100vh",
                    flex:1,
                    flexDirection:"row",
                    flexWrap:"wrap",
                    alignContent:"strech"
                    }}
                >
{/* this is the start */}
                {/* <div id="recaptcha-container" style={{backgroundColor:"green",height:"100vh"}}></div> */}
{/* this is the end */}
                <div className="gridDash">
                <Button ghost className="ghost" onClick={()=>browserHistory.push('/loginEmail')}>Sign In With Email</Button>
                </div>
                <div className="gridDash">
                <Button ghost className="ghost" onClick={()=>browserHistory.push('/registerPhone')}>Sign In With Phone Number</Button>
                </div>
                <div className="comments" >
                
                <Input.TextArea
                style={{borderRadius:25,backgroundColor:"#626268",color:"white"}}
                rows="7"
                placeholder="Any suggestions or queries"
                />
                <br/>
                <br/>
                <Button ghost id="submit">Submit</Button>                
                </div>
            </div>
            </div>
            </div>
        )
    }
}
export default Dashboard