import React ,{Component} from 'react';
import {Icon} from 'antd'
import {Router,Route,browserHistory} from 'react-router'
import  LoginEmail from './containers/loginEmail'
import RegisterEmail from './containers/registerEmail'
import Dashboard from './containers/dashboard'
import RegisterPhone from './containers/registerPhone'
import VerifyCode from './containers/verifyCode'
import TruckerDashboard from './containers/truckerDashboard'
import RiderBusinessDetails from './containers/riderBusinessDetails'
import RiderDashboard from './containers/riderDashboard'
import CreateOrder from "./containers/createOrder"
import firebase from 'firebase'
import store from './store';
import authActions from './store/actions/authActions'
import RatingComponent from './containers/rating';
const Truck =()=>(<div>Truck</div>)
const Business =()=>(<div>Rider Dashboard underconstruction</div>)

class Navigation extends Component{
    constructor(){
        super();
        this.state={
            Loading:true
        }
    }
    componentWillMount(){
        firebase.auth().onAuthStateChanged((user)=>{
            if(user){
                console.log(user,"this is user",user.uid)
                firebase.database().ref("user").child(user.uid).once("value",(snap)=>
                {console.log(snap.val(),"this sympbol")
                var obj = snap.val();
                obj.businessDetail?
                browserHistory.push(`/${obj.type}`):
                obj.type  === "trucker"?
                browserHistory.push(`/${obj.type}`):                
                browserHistory.push('/riderInfo ')
                this.setState({
                    Loading:false
                })
                }
               )
                
            }
            else {
                this.setState({Loading:false})
            }
        })

    }
    render(){
        return(<div>
            {this.state.Loading? 
            <center><Icon type="loading" style={{ fontSize: 50, color: '#453E4E'}} /></center>:
            <Router history={browserHistory}>
            <div>
            <Route path="/" component={Dashboard}/>
            <Route path="/loginEmail" component={LoginEmail}/>
            <Route path="/registerEmail" component={RegisterEmail}/>
            <Route path="/trucker" component={TruckerDashboard}/>
            <Route path="/registerPhone" component={RegisterPhone}/>
            <Route path="/riderInfo" component={RiderBusinessDetails}/>
            <Route path="/rider" component={RiderDashboard}/>
            <Route path="/verifyCode" component={VerifyCode}/>
            <Route path="/rating/:id" component={RatingComponent}/>            
            </div>
            </Router>
            }
            </div>
        )
    }
}

export default Navigation