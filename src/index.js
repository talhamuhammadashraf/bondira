import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import firebase from 'firebase'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

  // // Initialize Firebase
  // var config = {
  //   apiKey: "AIzaSyBEyvq6orAN3V_9tswPaj9O4CAwBHxRWLg",
  //   authDomain: "blazing-heat-7297.firebaseapp.com",
  //   databaseURL: "https://blazing-heat-7297.firebaseio.com",
  //   projectId: "blazing-heat-7297",
  //   storageBucket: "blazing-heat-7297.appspot.com",
  //   messagingSenderId: "912168317097"
  // };
  // firebase.initializeApp(config);
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyB2hCXu_HndpdSrhbEP6XobGT73nyTgJcs",
      authDomain: "react-prac-8e4a5.firebaseapp.com",
      databaseURL: "https://react-prac-8e4a5.firebaseio.com",
      projectId: "react-prac-8e4a5",
      storageBucket: "react-prac-8e4a5.appspot.com",
      messagingSenderId: "650862483596"
    };
    firebase.initializeApp(config)


ReactDOM.render(
  <MuiThemeProvider>
<App />
</MuiThemeProvider>
, document.getElementById('root'));
registerServiceWorker();
